const express = require('express');
const path = require('path');
const Usuario = require('../controlador/controlador.usuario');
const Trabajador = require('../controlador/controlador.trabajador');
const User = require('../modelo/usuario');
const Trab = require('../modelo/trabajador');

const app = express();
let inicio = express.Router();
let trabajador = express.Router();


app.use('/', trabajador); //mirar
app.use('/', inicio);


//PAGINA DE INICIO
inicio.get('/', (req, res)=>{
    
    res.render('index');
    
});

//LOGIN
inicio.get('/login', (req, res)=>{
    res.render('login', {
        mensaje:req.session.mensaje
    });
    
});
inicio.post('/login', Usuario.iniciarSesion);

//SIGNUP
inicio.get('/signup', (req, res)=>{
    
    res.render('signup', {
        mensaje:req.session.mensaje
    });
    
});
inicio.post('/signup', Usuario.registrarse);

//PROFILE
inicio.get('/profile', isLoggedIn,  (req, res)=>{
    User.findOne({'email':req.session.user}, (err, data)=>{
        if(data){
            // console.log(data);
            res.render('profile',{
                user:data
            });
        }
    })
    
});
//ELIMINAR TRABAJADOR
trabajador.get('/profile/eliminar', isLoggedIn, (req, res)=>{
    res.render('eliminar', {mensaje:req.mensaje})
})
trabajador.post('/profile/eliminar', isLoggedIn, Trabajador.eliminar);

//EDITAR TRABAJADOR
trabajador.get('/profile/editar', isLoggedIn, (req, res)=>{
    res.render('editar', {mensaje:req.mensaje})
})
trabajador.post('/profile/editar', isLoggedIn, Trabajador.editar);
//CREAR TRABAJADOR
trabajador.get('/profile/crear', isLoggedIn, (req, res)=>{
    res.render('crear', {mensaje:req.mensaje})
})
trabajador.post('/profile/crear', isLoggedIn, Trabajador.crear);

//VER TRABAJADOR 
trabajador.get('/profile/ver', isLoggedIn, Trabajador.ver);




//LOG OUT
//añadir logout en html y metodo para desconectar el usuario.
inicio.get('/logout', (req, res) => {
    console.log('El usuario: '+ req.session.user + ' ha salido de la app.')
    req.session.destroy();
    
    res.redirect('/');
});


module.exports = app;

//COMPROBAMOS SI EL USUARIO ESTÁ LOGGEADO  (mirar +)



function isLoggedIn (req, res, next) {
    if (req.session && User.findOne({'email':req.session.user}) && req.session.user!=null){
        console.log('El usuario '+req.session.user+' ha entrado en la app.')
        return next();
    }
    console.log('Se ha intentado entrar con: '+ req.session.user)
    res.redirect('/');
      
}


