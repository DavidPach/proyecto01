const Usuario = require('../modelo/usuario');

const path = require('path');


/*=============================================
EXPORTAMOS LAS FUNCIONES DEL CONTROLADOR
=============================================*/


/*=============================================
SIGN UP
=============================================*/
let registrarse = (req, res)=>{
    let body = req.body;
    
    let usuario = new Usuario();
    
    usuario.nombre = body.nombre;
    usuario.apellido = body.apellido;
    usuario.telefono= body.telefono
    usuario.email=body.email;
    usuario.password= usuario.generateHash(body.password);
    
    if(comprobarSingup(body, req, 'registrar')==false){ 
       
        return  res.redirect('/signup');
    }  //Comprobamos campos vacios y si la password repetida es igual
    

    //Busca un usuario en la DB y si lo encuentra salta error, si no, prosigue
    Usuario.findOne({'email':usuario.email}, (err, data)=>{
       if(err){
           return res.redirect('/');
       }
       if(data){
            req.session.mensaje = "Ya existe un usuario con ese email";
            return res.redirect('/signup');
        
       }else{
           //Salvamos el user en la DB
           usuario.save((err, data)=>{
               if(err){
                    req.session.mensaje = "Error al guardar en la DB";
                    return res.redirect('/signup');
       
               }
               req.session.mensaje="";
               req.session.user = body.email;   //Guardamos el email del usuario para mantener la sesion iniciada
               res.redirect('/profile');

           })
       }
       
    })

}

/*=============================================
LOGIN
=============================================*/
let iniciarSesion = (req, res)=>{
    let body = req.body;

    if(comprobarSingup(body, req, 'login')==false){ 
        return  res.redirect('/login');
    } 
    
    let params = {};
    params['email'] = body.email;
    console.log(params);

    Usuario.findOne(params,  (err, data)=>{
        if(err){
            req.session.mensaje = "Error al validar, intentelo de nuevo mas tarde.";
            console.log(err);
            return res.redirect('/');
        }
            //Comprobamos si existe el usuaior introducido
        if(!data){
            req.session.mensaje = "El usuario no existe.";
            return res.redirect('/login');
        }
            //Funcion que comprueba si la contraseña es igual
        if(!data.validatePassword(body.password)){ 
            req.session.mensaje = "La contraseña es incorrecta.";
            return res.redirect('/login');
        }
        //Pasar la data a .json() y ponerla en el archivo "seseion.json"
        
        // let persona = persona.json({
        //     data
        // })
        req.session.mensaje="";
        req.session.user = body.email;      //Guardamos el email del usuario para mantener la sesion iniciada
        return res.redirect('/profile');
    })
}


/*=============================================
OTRAS FUNCIONES
=============================================*/

//función para comprobar contenido (Login y SignUp)
function comprobarSingup(body, req, tipo){
    console.log(body.nombre);
    if(body.nombre == ""){
        req.session.mensaje = "El campo /Nombre/ está vacio";
        return false;
    }
    if(body.apellido == ""){
        req.session.mensaje = "El campo /Apellido/ está vacio";
        return false;
    }
    if(body.telefono == ""){
        req.session.mensaje = "El campo /Telefono/ está vacio";
        return false;
    }
    if(body.email == ""){
        req.session.mensaje = "El campo /Email/ está vacio";
        return false;
    }
    if(body.password == ""){
        req.session.mensaje = "El campo /Contraseña/ está vacio";
        return false;
    }
    if(body.password2 == ""){
        req.session.mensaje = "El campo /Repita la contraseña/ está vacio";
        return false;
    }
    if(body.password != body.password2 && tipo == 'registrar'){  //En caso de que nos estemos logeando este campo lo salta.
        req.session.mensaje = "Las contraseñas no coinciden";
        return false;
    } 
    return true;
}



module.exports = {
    registrarse,
    iniciarSesion

}