const Trabajador = require('../modelo/trabajador');
const path = require('path');
const serviTrabajador = require('../service/service.trabajador');



let editar = async(req, res) =>{
    try{
        await serviTrabajador.editarTrabajador(req);
        res.redirect('/profile/ver');
    }catch(err){
        res.render('editar', {mensaje:err})
    }
}    
let eliminar = async(req, res)=>{
    try{
        await serviTrabajador.eliminarTrabajador(req);
        res.redirect('/profile/ver');
    }catch(err){
        res.render('eliminar', {mensaje:err})
    }

}
let ver = async(req, res)=>{

    try{
        let data = await serviTrabajador.verTrabajador(req);
        res.render('ver',{
            mensaje:data.mensaje,
            data:data.data
        })
    }catch(err){
        res.render('ver',{
            mensaje:err.mensaje,
            data:err.data
        })
    }
}

let crear = async(req, res)=>{

    try{
        await serviTrabajador.crearTrabajador(req);
        res.redirect('/profile/ver');
    }catch(err){
        res.render('crear', {mensaje:err});
    }
}
module.exports = {
    ver,
    crear, 
    editar,
    eliminar

}