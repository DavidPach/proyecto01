
'use strict'

const mongoose = require("mongoose");
const bcrypt = require("bcrypt");


let Schema = mongoose.Schema;

let userSchema = new Schema({
    nombre:{
        type:String,
        required: [true, "El nombre es obligatorio"]
    },
    apellido:{
        type:String,
        required: [true, "El apellido es obligatorio"]
    },

    telefono:{
        type: Number,
        required: [true, "El telefono es obligaotrio"],
        unique: true

    },
    email:{
        type:String,
        required: [true, "El usuario es obligatoria"],
        unique: true
    },
    
    password:{
        type:String,
        required: [true, "La contraseña es obligatoria"]
    }

})

//genera el hash de la contraseña
userSchema.methods.generateHash = function(password){
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
}

//Compara las contraseñas
userSchema.methods.validatePassword =function(password){  
    return bcrypt.compareSync(password, this.password);
}
/* EXPORTAR EL MODELO */

module.exports= mongoose.model("usuarios", userSchema);