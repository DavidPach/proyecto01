
'use strict'

const mongoose = require("mongoose");



let Schema = mongoose.Schema;

let userSchema = new Schema({
    id:{
        type:Number,
        required: false
    },
    name:{
        type:String,
        required: true
    },
    username:{
        type:String,
        required: true,
        unique:true
    },
    email:{
        type:String,
        required:true,
        unique:true
    },
    address:{
        street:{
            type:String,
            required: true
        },
        suite:{
            type:String,
            required: true
        },
        city:{
            type:String,
            required: true
        },
        zipcode:{
            type:String,
            required: true
        },
        geo:{
            lat:{
                type:String,
                required: false
            },
            lng:{
                type:String,
                required: false
            }
        }
    },
    phone:{
        type:String,
        required: true
    },
    website:{
        type:String,
        required: true
    },
    company:{
        name:{
            type:String,
            required: true
        },
        catchPhrase:{
            type:String,
            required: true
        },
        bs:{
            type:String,
            required: true
        }
    }

})



/* EXPORTAR EL MODELO */

module.exports= mongoose.model("trabajadores", userSchema);