const Trabajador = require('../modelo/trabajador');

let buscarDentro = (params, req)=>{
    //  Comprobamos si existen en la DB propia  (CAMBIAR)
    return new Promise((resolve, reject)=>{
        Trabajador.findOne( params ,(err, data)=>{
            if(err){
                req.mensaje = "Error en la búsqueda interna";
                console.log(err);
            }
                //Comprobamos si existe el usuaior introducido
            if(!data){
                req.mensaje = "No se encontraron datos internos";
                reject('No se encontraron datos internos'); 
            }else{
                req.mensaje = "El usuario se encuentra en la DB";
                resolve(data);
            }
            
        });
    })
}
let subirYmostrar = (uno, req)=>{

    return new Promise((resolve, reject)=>{
        

        uno.company.name=uno.company.name.toUpperCase();
        let trab = new Trabajador(uno);
        
        
        trab.save((err, data)=>{
            if(err){
                req.mensaje = 'Error al subir a la DB';
                reject(err);
            }else{
                req.mensaje='';
                resolve(data)
            }
        })
        
    })
}

let actualizarDB = (req, res) =>{
    return new Promise((resolve, reject)=>{
        Trabajador.findOneAndUpdate({'email':req.body.email}, {
            $set: {
                name:req.body.dato
                }
            }   ,(err, data)=>{
                if(err){
                    req.mensaje='Error en la DB';
                    console.log(err); 
                }
                if(!data){
                    req.mensaje = 'No existe el trabajador especificado.';
                    console.log(req.mensaje);
                    reject(req.mensaje);
                }else{
                    req.mensaje ='';
                    
                    // console.log(data);
                    resolve();
                }
            })
    })
}
let eliminarDB = (req, res)=>{
    return new Promise ((resolve, reject)=>{
        Trabajador.findOneAndRemove({'email':req.body.email}, (err, data)=>{
            if(err){
                req.mensaje='Error en la DB';
                console.log(err);
                
            }
            if(!data){
                reject('No existe el trabajador especificado.');
            }else{
                resolve();
                
            }
        })
    })

}

module.exports = {
    buscarDentro,
    subirYmostrar,
    actualizarDB,
    eliminarDB
}