const Trabajador = require('../modelo/trabajador');
const serviMongo = require('../service/service.mongo');
const serviPlaceholder = require('../service/service.placeholder');

const path = require('path');
const { resolve } = require('path');

let comprobarPost = (req) =>{
    let body= req.body;
    return new Promise((resolve, reject)=>{
        if(body.name == ""){
            
            reject('El campo nombre es obligatorio');
        }
        if(body.username == ""){
            
            reject('El campo username es obligatorio');
        }
        if(body.email == ""){
            
            reject('El campo email es obligatorio');
        }
        if(body.phone == ""){
            
            reject('El campo telefono es obligatorio');
        }
        if(body.website == ""){
            
            reject('El campo sitio web es obligatorio');
        }
        if(body.street == ""){
            
            reject('El campo calle es obligatorio');
        }
        if(body.suite == ""){
            
            reject('El campo nº es obligatorio');
        }
        if(body.city == ""){
            
            reject('El campo ciudad es obligatorio');
        }
        if(body.zipcode == ""){
            
            reject('El campo código postal es obligatorio');
        }
        if(body.company_name == ""){
            
            reject('El campo empresa es obligatorio');
        }
        if(body.catchPhrase == ""){
            
            reject('El campo lema es obligatorio');
        }
        if(body.bs == ""){
            
            reject('El campo bs es obligatorio');
        }
        else{
            let trab = new Trabajador();
            trab.name = body.name;
            trab.username = body.username;
            trab.email = body.email;
            trab.phone = body.phone;
            trab.website = body.website;
            trab.address.street = body.street;
            trab.address.suite = body.suite;
            trab.address.city = body.city;
            trab.address.zipcode = body.zipcode;
            trab.company.name = body.company_name;
            trab.company.catchPhrase= body.catchPhrase;
            trab.company.bs = body.bs;
            req.mensaje='';
            resolve(trab);

        }
    })

}


let comprobarTexto = req =>{
    return new Promise ((resolve, reject)=>{
       if(req.query.dato==null || req.query.dato == ""){
           req.mensaje="No se ha introducido ningún campo.";
           reject("No se ha introducido ningun campo")
       }
      let varDatos='';
       //Comprobamos si hay opción.  (CAMBIAR)
      if(req.query.opcion == 0){
           req.mensaje="No hay opciones seleccionadas";
           reject("No hay opciones");
      }else{
          if(req.query.opcion==1){varDatos='name'}
          if(req.query.opcion==2){varDatos='username'}
          if(req.query.opcion==3){varDatos='phone'}
          if(req.query.opcion==4){varDatos='website'}
      }
   
       //Comprobamos si hay opción.
       let params = {};
       
       params[varDatos] = req.query.dato;
       
       resolve(params);
   })
}
let verTrabajador = (req, res) =>{
    return new Promise(async (resolve, reject)=>{
        prueba = new Trabajador ();
        try{
            let params = await comprobarTexto(req);
            try{
                let data = await serviMongo.buscarDentro(params, req);
                req.mensaje="";
                resolve({mensaje:req.mensaje, data:data });
            }catch{
                data = await serviPlaceholder.buscarFuera(params, req);
                try{
                    data = await serviMongo.subirYmostrar(data, req);
                    resolve({mensaje:req.mensaje, data:data });
                }catch{
                    reject({mensaje:req.mensaje, data:prueba});
                }
            }
        }catch{
            reject({mensaje:req.mensaje, data:prueba});
        }
    })
} 

let crearTrabajador = (req, res)=>{
    return new Promise(async(resolve, reject)=>{
        try{
            let data = await comprobarPost(req);
            let params = {};
            params['email'] =data.email;
            try{    
                await serviMongo.buscarDentro(params, req);
                reject(req.mensaje);
            }catch{
                try{
                    data = await serviPlaceholder.buscarFuera(params, req);
                    try{
                        await serviMongo.subirYmostrar(data, req);
                        resolve();
                    }catch(err){
                        reject(err);
                    }
                }catch{
                    await serviMongo.subirYmostrar(data, req);
                    resolve();
                }
            }
        }catch(err){
            reject(err);
        }
    })
}

// let crearTrabajador = (req, res)=>{
//     return new Promise((resolve, reject)=>{
//         comprobarPost(req).then((data)=>{
//             console.log(data);
//             let params = {};
//             params['email'] =data.email;
//             console.log(params);
    
//             serviMongo.buscarDentro(params, req).then(()=>{
//                 console.log(req.mensaje);
//                 reject(req.mensaje);
  
//             }).catch(()=>{
//                 serviPlaceholder.buscarFuera(params, req).then((existe)=>{
//                     console.log(existe);
//                     serviMongo.subirYmostrar(existe, req).then(()=>{
//                         resolve();
    
//                     }).catch(err=>{
//                         console.log(err);
//                         reject(err)
                        
//                     })
    
//                 }).catch(err=>{
//                     console.log(err);
    
//                     serviMongo.subirYmostrar(data, req).then(()=>{
//                         resolve();
    
//                     }).catch(err=>{
//                         console.log(err);
//                         reject(err);
                        
//                     })
//                 })
                
//             })
//         }).catch(err=>{
//             console.log(err);
//             reject(err);
//         })
//     })
// }

let eliminarTrabajador = (req, res)=>{
    return new Promise(async(resolve, reject)=>{
        try{
            await serviMongo.eliminarDB(req);
            resolve();
        }catch(err){
            reject(err);
        }
       
    })
}
let editarTrabajador = (req, res)=>{
    return new Promise(async(resolve, reject)=>{
        try{
            await serviMongo.actualizarDB(req);
            resolve();
        }catch(err){
            reject(err);
        }
        
       
    })
}


module.exports = {
    comprobarTexto,
    verTrabajador,
    comprobarPost,
    crearTrabajador,
    editarTrabajador,
    eliminarTrabajador

}