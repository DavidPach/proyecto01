require('./config/config');

const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const path = require('path');
const morgan = require('morgan');
const session = require('express-session');
const flash = require('connect-flash');

const app = express();

/* MIDDWARE */
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json())

app.set('view', path.join(__dirname, '/views'));
app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);


app.use(morgan('dev'));

app.use(session({
    secret: 'ihuhjkhuigb',
    resave: false,
    saveUninitialized: false
}));

app.use(flash());

/* RUTAS */
app.use( require('./rutas/ruta'));

/* STATIC FILES */
app.use(express.static(path.join(__dirname, 'public')));

/* Conexión a la DB */
mongoose.connect('mongodb://localhost:27017/visiotech',{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,

},  (err, res)=>{

	if(err) throw err;

	console.log("Conectado a la base de datos")

});

/*=============================================
SALIDA PUERTO HTTP
=============================================*/
app.listen(process.env.PORT, ()=>{

	console.log(`Habilitado el puerto ${process.env.PORT}`)
})